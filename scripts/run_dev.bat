@echo off

:: GUI Initialization
cd ../gui
start "analysis-system-gui-process" yarn install ^&^& yarn run serve ^&^& exit

:: API Initialization
cd ../api
if not exist venv/ (
    py -3 -m venv venv
)

cd venv/Scripts/
call activate.bat
cd ../../
python -m pip install -r requirements.txt

cd ../

:: TODO: API Environment vars
flask run

taskkill /FI "WindowTitle eq analysis-system-gui-process*" /T /F