@echo off

:: Do API Build
cd ../api/venv/Scripts/
call activate.bat
cd ../../
python -m pip install -r requirements.txt
cd ../

pyinstaller --name "api" --add-data "api/flaskr/sql/internal.db;./flaskr/sql/" --add-data "api/flaskr/sql/initialize.sql;./flaskr/sql/" "./api/app.py" --noconfirm

:: Do GUI Build

