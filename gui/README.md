# Fino User Interface

Simples sistema de analise de dados. (interface)

-----------------------------------------------------------

## Instalando dependências
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
yarn run serve
OR
yarn run electron:serve
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
yarn run build
OR
yarn run electron:build
```