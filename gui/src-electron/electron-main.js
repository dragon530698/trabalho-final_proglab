import { app, BrowserWindow, nativeTheme } from 'electron'
import path from 'path'
import { stdout } from 'process'

try {
  if (process.platform === 'win32' && nativeTheme.shouldUseDarkColors === true) {
    require('fs').unlinkSync(require('path').join(app.getPath('userData'), 'DevTools Extensions'))
  }
} catch (_) { }

const root = path.dirname(app.getAppPath())

const distFolder = path.join(root, 'api/')
const apiExecWin = path.join(distFolder, 'api.exe')
const apiExec = path.join(distFolder, 'api')

let mainWindow
let pyProc = null

const getScriptPath = () => {
    if (process.platform === 'win32') {
        return apiExecWin
    }
    return apiExec
}

const runApi = () => {
  let script = getScriptPath()
  let port = 5000
  
  stdout.write('Starting API on port ' + port + '...\n')
  pyProc = require('child_process').execFile(script, [port])
  
  if (pyProc != null) {
    stdout.write('API success on port ' + port + '\n')
    stdout.write('Executable: ' + script+ "\n")
  }
}


function createWindow () {
  if (process.env.NODE_ENV == 'production') {
    runApi()
  }
  
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    width: 1280,
    height: 720,
    useContentSize: true,
    webPreferences: {
      contextIsolation: true,
      // More info: /quasar-cli/developing-electron-apps/electron-preload-script
      preload: path.resolve(__dirname, process.env.QUASAR_ELECTRON_PRELOAD)
    }
  })

  mainWindow.loadURL(process.env.APP_URL)

  // mainWindow.webContents.openDevTools()
  // if (process.env.DEBUGGING) {
  //   // if on DEV or Production with debug enabled
  //   mainWindow.webContents.openDevTools()
  // } else {
  //   // we're on production; no access to devtools pls
  //   mainWindow.webContents.on('devtools-opened', () => {
  //     mainWindow.webContents.closeDevTools()
  //   })
  // }

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    pyProc.kill('SIGTERM')
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})
