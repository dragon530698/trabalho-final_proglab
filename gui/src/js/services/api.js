export const BASE_URL = "http://127.0.0.1:5000";

export async function api(url, params = {}) {
    params.headers = Object.assign({"Content-Type": "application/json"}, params.headers)

    let response = await fetch(BASE_URL + url, params);
    let json = (await response.json()) || {};
    if (!response.ok) {
        let errorMessage = json.error
        ? json.error.error || json.error
        : response.status;
        throw new Error(errorMessage);
    }
    return json;
}
