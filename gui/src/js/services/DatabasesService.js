import { api } from "./api";

export default {
    getDatabases() {
        return api("/external_db/db", {
            method: "GET"
        })
    },
    newDatabase(databaseData) {
        return api("/external_db/db", {
            method: "POST",
            body: JSON.stringify(databaseData)
        })
    },
    updateDatabase(databaseData) {
        return api("/external_db/update_db", {
            method: "PUT",
            body: JSON.stringify(databaseData)
        })
    },
    deleteDatabase(dbId) {
        return api("/external_db/delete_db/" + dbId, {
            method: "DELETE"
        })
    },
    runQuery(tableId) {
        return api("/external_db/run_query/" + tableId, {
            method: "GET"
        })
    }
}