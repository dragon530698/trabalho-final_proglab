import { api } from "./api";

export default {
    // Dashboards
    getDashboards() {
        return api("/main/dashboards", {
            method: "GET"
        })
    },
    newDashboard(dashboardData) {
        return api("/main/dashboards", {
            method: "POST",
            body: JSON.stringify(dashboardData)
        })
    },
    updateDashboard(dashboardData) {
        return api("/main/dashboards", {
            method: "PUT",
            body: JSON.stringify(dashboardData)
        })
    },
    deleteDashboard(id_dashboard) {
        return api("/main/dashboards/" + id_dashboard, {
            method: "DELETE"
        }) 
    },

    // Datatabes
    getDatatables(id_dashboard) {
        return api("/main/datatable/" + id_dashboard, {
            method: "GET"
        })
    },
    newDatatable(tableData) {
        return api("/main/datatable", {
            method: "POST",
            body: JSON.stringify(tableData)
        })
    },
    updateDatatable(tableData) {
        return api("/main/datatable", {
            method: "PUT",
            body: JSON.stringify(tableData)
        })
    },
    deleteDatatable(id_datatable) {
        return api("/main/datatable/" + id_datatable, {
            method: "DELETE"
        }) 
    }
}