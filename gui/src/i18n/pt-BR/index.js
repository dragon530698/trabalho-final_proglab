export default {
  notFound: {
    text: "Oops. Nothing here...",
    btn: "Go home"
  },
  index: {
    start: "Iniciar"
  },
  settings: {
    tabs: {
      databases: {
        tabName: "Bancos de Dados",
        form: {
          empty: "Selecione um Banco de Dados ou um Tipo de Banco.",
          dbSelect: "Banco Selecionado",
          dbTypeSelect: "Tipo de Banco",
          dbName: "Nome do Banco",
          defaultDbName: "Novo Banco de Dados",
          btn: {
            submit: "Confirmar",
            reset: "Limpar",
            delete: "Deletar"
          },
          deleteDBDialog: {
            title: "Você tem certeza?",
            message: "Você realmente quer deletar {dbName}?"
          }
        }
      }
    }
  },
  dashboardMenu: {
    boardsHeader: "Dashboards",
    editBoard: "Alterar Dashboard",
    deleteBoard: "Deletar Dashboard",
    newBoardBtn: {
      label: "Nova Dashboard",
      desc: "Adicione uma nova dashboard."
    },
    boardDialog: {
      title: "Dashboard",
      defaultName: "Nov Dashboard",
      defaultDescription: "Essa é minha dashboard.",
      boardName: "Nome da Dashboard",
      boardDesc: "Descrição",
      btn: {
        cancel: "Cancelar",
        confirm: "Confirmar"
      }
    }
  },
  dashboard: {
    newTable: "Nova Tabela",
    runQueries: "Executar Consultas",
    defaultTableName: "Nova Tabela",
    deleteBoardDialog: {
      title: "Você tem certeza?",
      message: "Você realmente quer deletar {boardName}?"
    }
  },
  datatable: {
    deleteTooltip: "Deletar tabela",
    editTooltip: "Alterar tabela",
    runTooltip: "Executar Consulta",
    noData: "Nenhum dado encontrado.",
    queryError: "Falha ao executar consulta: {errorMsg}",
    deleteDialog: {
      title: "Você tem certeza?",
      message: "Você realmente quer deletar {tableName}?"
    },
    editDialog: {
      nameField: 'Nome da tabela',
      dbSelect: 'Banco Selecionado',
      sqlQueryLabel: "Consulta SQL:",
      btn: {
        cancel: "Cancelar",
        confirm: "Confirmar Mudanças"
      }
    }
  }
}
