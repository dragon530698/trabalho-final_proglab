export default {
  notFound: {
    text: "Oops. Nothing here...",
    btn: "Go home"
  },
  index: {
      start: "Start"
  },
  settings: {
    tabs: {
      databases: {
        tabName: "Databases",
        form: {
          empty: "Select a Database or a Database Type.",
          dbSelect: "Selected Database",
          dbTypeSelect: "Database Type",
          dbName: "Database Name",
          defaultDbName: "New Database",
          btn: {
            submit: "Submit",
            reset: "Reset",
            delete: "Delete"
          },
          deleteDBDialog: {
            title: "Are you sure?",
            message: "Do you really want to delete {dbName}?"
          }
        }
      }
    }
  },
  dashboardMenu: {
    boardsHeader: "Dashboards",
    deleteBoard: "Delete Dashboard",
    editBoard: "Edit Dashboard",
    newBoardBtn: {
      label: "New Dashboard",
      desc: "Add a new dashboard."
    },
    boardDialog: {
      title: "Dashboard",
      defaultName: "New Dashboard",
      defaultDescription: "This is my dashboard.",
      boardName: "Dashboard name",
      boardDesc: "Description",
      btn: {
        cancel: "Cancel",
        confirm: "Confirm"
      }
    }
  },
  dashboard: {
    newTable: "New Table",
    runQueries: "Run Queries",
    defaultTableName: "New Table",
    deleteBoardDialog: {
      title: "Are you sure?",
      message: "Do you really want to delete {boardName}?"
    }
  },
  datatable: {
    deleteTooltip: "Delete table",
    editTooltip: "Edit table",
    runTooltip: "Run Query",
    noData: "No data found.",
    queryError: "Failed to run query: {errorMsg}",
    deleteDialog: {
      title: "Are you sure?",
      message: "Do you really want to delete {tableName}?"
    },
    editDialog: {
      nameField: 'Table name',
      dbSelect: 'Selected Database',
      sqlQueryLabel: "SQL Query:",
      btn: {
        cancel: "Cancel",
        confirm: "Confirm Changes"
      }
    }
  }
}
