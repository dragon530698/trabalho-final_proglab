
const routes = [
  {
    name: 'index',
    path: '/',
    component: () => import('pages/Index.vue')
  },
  {
    name: 'main',
    path: '/main',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { name: 'initial', path: '', component: () => import('pages/Main.vue') },
      { name: 'settings', path: 'settings', component: () => import('pages/Settings.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
