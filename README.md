# Fino

Simples sistema de analise de dados.

-----------------------------------------------------------
# Pre requisitos

- Node.JS
- Yarn
- Python (e pip)

Tanto o `yarn` quanto o `python` devem poder ser utilizados do terminal (estarem na variavel PATH, no Windows.)


-----------------------------------------------------------
# Setup

Essa aplicação consiste de duas partes:
- API: Escrita em Python usando Flask, lida com a conexão para o DB interno e para DB externos.
- GUI: Escrita em JS usando Quasar, framework baseado em Vue, que é baseado em Node.JS.

Dentro da pasta `scripts`, há o arquivo `run_dev.bat`;

Esse script inicia o projeto - tanto a GUI(porta 8080) quanto a API(porta 5000) - enquanto instalando as dependências.

É possivel também iniciar os projetos individualmente. Mais informações dentro do readme de cada modulo.