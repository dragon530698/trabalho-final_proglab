import sqlite3
from pathlib import PurePath

from flask import current_app, g

"""
Module that handles all connection with the internal database.
Uses the flask g object. See https://flask.palletsprojects.com/en/2.0.x/appcontext/

Returns
-------
    db (sqlite3.Connection): A connection to the internal database.
"""


def get_db():
    """
    Gets internal database, or creates a connection if there is none.
    Uses the flask "g" object to store the connection to use within contexts.

    Returns
    -------
    db : sqlite3.Connection
        an instance of the connection to the database
    """

    if 'db' not in g:
        db_path = PurePath(current_app.config['DATABASE']).as_uri() + '?mode=rwc'
        g.db = sqlite3.connect(
            db_path,
            detect_types=sqlite3.PARSE_DECLTYPES,
            uri=True
        )
        g.db.row_factory = sqlite3.Row
        
    return g.db


def close_db(*_):
    """
    Closes the connection to the database, and removes the connection from the
    flask g object. Called on the end of an application context.

    Returns
    -------
    None
    """
    db = g.pop('db', None)

    if db is not None:
        db.close()


def init_db():
    """
    Initializes database using the sql/initialize.sql script (creates tables);
    Also registers closing the connection on the end of an application context.

    Returns
    -------
    db : sqlite3.Connection
        an instance of the connection to the database
    """
    db = get_db()

    with current_app.open_resource('sql/initialize.sql') as f:
        db.executescript(f.read().decode('utf8'))

    current_app.teardown_appcontext(close_db)
    return db