from flaskr.external_db.external_db_handler import ExternalDBHandler
from flask import (
    Blueprint, request, jsonify, current_app
)

from flaskr.db import get_db

###################
bp = Blueprint('external_db', __name__, url_prefix='/external_db')

@bp.before_request
def before_request():
    # Activate foreign keys pragma (sqlite exclusive feature)
    # This is needed to activate cascade delete support.
    # This needs to be done EVERY TIME A QUERY USES FKS.
    # See item 2 at https://sqlite.org/foreignkeys.html
    # Also see https://stackoverflow.com/questions/65654550/foreign-key-constraint-is-not-working-properly-on-sqlite
    db = get_db()
    db.execute("PRAGMA foreign_keys = ON;")
    db.commit()

@bp.after_request
def after_request(response):
    origin_whitelist= ['http://localhost:8080']
    if request.headers.get('Origin') in origin_whitelist:
        response.headers['Access-Control-Allow-Origin'] = request.headers['Origin'] 
        response.headers['Access-Control-Allow-Methods'] = 'PUT,GET,POST,DELETE'
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type,Authorization'
    return response

####################
# Routes

@bp.route('/db', methods={'POST'})
def create_external_db():
    db = get_db()
    error = None

    db_name = request.json['db_name']
    db_type = request.json['db_type']

    try:
        cur = db.cursor()
        cur.execute(
            "INSERT INTO external_dbs (name, db_type) VALUES (?, ?);",
            (db_name, db_type)
        )
        id_db = cur.lastrowid
        db.commit()

        ExternalDBHandler().create_db(id_db, **request.json)
        return ({'id_db': id_db}, 200)
    except ValueError:
        error = 'Database type not found!'
    # except IntegrityError || ProgrammingError as e:
    except db.Error as e:
        error = 'Error when inserting into databases table:\n' + str(e)
    
    if error is not None:
        current_app.logger.error(error)
        return({'error': error}, 500)

@bp.route('/db', methods={'GET'})
def get_external_db():
    db = get_db()
    error = None

    try:
        cur = db.cursor()
        data = cur.execute("SELECT id_db, name, db_type FROM external_dbs;").fetchall()
        res = []
        for entry in data:
            res.append({
                'id_db': entry[0],
                'name': entry[1],
                'db_type': entry[2]
            })
        
        return jsonify(res)
    except db.Error as e:
        error = 'Error when retrieving external databases:\n' + str(e)
    
    if error is not None:
        current_app.logger.error(error)
        return({'error': error}, 500)

@bp.route('/update_db', methods={'PUT'})
def update_external_db():
    # TODO: Add database type update
    db = get_db()
    error = None

    new_db_name = request.json['db_name']
    # db_type = request.json['db_type']
    id_db = request.json['id_db']

    try:
        cur = db.cursor()
        old_db_name = cur.execute("""
            SELECT name FROM external_dbs WHERE id_db = :id_db;
        """, { "id_db": id_db } ).fetchone()[0]

        cur.execute("""
            UPDATE external_dbs
            SET name = :name
            WHERE id_db = :id_db;
            """,
            {
                "name": new_db_name,
                # "db_type": db_type,
                "id_db": id_db
            })
        
        db.commit()

        ExternalDBHandler().update_db(old_db_name, new_db_name, **request.json)
        return ('', 204)
    except ValueError:
        error = 'Database type not found!'
    # except IntegrityError || ProgrammingError as e:
    except db.Error as e:
        error = f'Error while updating database with id {id_db}:\n' + str(e)
    
    if error is not None:
        current_app.logger.error(error)
        return({'error': error}, 500)

@bp.route('/delete_db/<int:id_db>', methods={'DELETE'})
def delete_external_db(id_db):
    db = get_db()

    try:
        cur = db.cursor()
        cur.execute("""
            DELETE FROM external_dbs
            WHERE id_db = :id_db;
            """,
            {
                "id_db": id_db
            })
        db.commit()

        ExternalDBHandler().delete_db(id_db)
        return ('', 204)
    # except IntegrityError || ProgrammingError as e:
    except db.Error:
        current_app.logger.error('Error when deleting from databases table!', exc_info=1)
        return('', 500)

@bp.route('/run_query/<int:table_id>', methods={'GET'})
def run_table_query(table_id):
    db = get_db()
    error = None

    try:
        # Get table query and target table id
        cur = db.cursor()
        table_data = cur.execute("SELECT query, id_db FROM datatables WHERE id_table = :table_id;",
                                    {"table_id": table_id}).fetchone()
        query = table_data[0]
        id_db = table_data[1]

        # Get table's target db
        db_data = cur.execute("SELECT name, db_type FROM external_dbs WHERE id_db = :id_db;",
                                        {"id_db": id_db}).fetchone()
        db_name = db_data[0]
        db_type = db_data[1]

        ex_db = ExternalDBHandler().get_db(db_name, db_type)

        data = None
        # Run query on target db
        if db_type == 'sqlite':
            ex_cur = ex_db.get_db().cursor()
            data = ex_cur.execute(query).fetchall()
        
        if len(data) > 0:
            rows = []

            for row in data:
                rows.append(tuple(row))

            res = {
                "columns": data[0].keys(),
                "rows": rows
            }
            return jsonify(res)
        else:
            # No data found.
            return({}, 201)

    except db.Error as e:
        current_app.logger.error('Error when running table query!', exc_info=1)
        return({'error': str(e)}, 400)

