from flask import (
    Blueprint, flash, request, jsonify, current_app
)

from flaskr.db import get_db

bp = Blueprint('main', __name__, url_prefix='/main')

@bp.before_request
def before_request():
    # Activate foreign keys pragma (sqlite exclusive feature)
    # This is needed to activate cascade delete support.
    # This needs to be done EVERY TIME A QUERY USES FKS.
    # See item 2 at https://sqlite.org/foreignkeys.html
    # Also see https://stackoverflow.com/questions/65654550/foreign-key-constraint-is-not-working-properly-on-sqlite
    db = get_db()
    db.execute("PRAGMA foreign_keys = ON;")
    db.commit()

@bp.after_request
def after_request(response):
    # Add frontend URL to whitelist for CORS
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
    origin_whitelist= ['http://localhost:8080']
    if request.headers.get('Origin') in origin_whitelist:
        response.headers['Access-Control-Allow-Origin'] = request.headers['Origin'] 
        response.headers['Access-Control-Allow-Methods'] = 'PUT,GET,POST,DELETE'
        response.headers['Access-Control-Allow-Headers'] = 'Content-Type,Authorization'
    return response


####################
# Routes
#
# Dashboard related routes

@bp.route('/dashboards', methods={'POST'})
def create_dashboard():
    error = None
    db = get_db()

    name = request.json['name']
    description = request.json['description']

    # TODO: Validate dashboard name in front-end
    if not name or name == '':
        error = 'Name cannot be empty.'
    
    if error is None:
        try:
            cur = db.cursor()
            cur.execute(
                "INSERT INTO dashboards (name, description) VALUES (?, ?);",
                (name, description)
            )
            db.commit()
            lastId = cur.lastrowid

            return ({'id_dashboard': lastId}, 200)
        except db.Error as e:
            current_app.logger.error('Error while creating new dashboard!', exc_info=1)
            return ({'error': str(e)}, 500)
    else:
        return ({'error': error}, 400)


@bp.route('/dashboards', methods={'GET'})
def get_dashboards():
    db = get_db()

    try:
        cur = db.cursor()
        data = cur.execute("SELECT * FROM dashboards;").fetchall()
        res = []
        for entry in data:
            res.append({
                'id': entry[0],
                'name': entry[1],
                'description': entry[2]
            })
        
        return jsonify(res)

    except db.Error as e:
        current_app.logger.error('Error while retrieving all dashboards!', exc_info=1)
        return ({'error': str(e)}, 500)


@bp.route('/dashboards', methods={'PUT'})
def update_dashboard():
    error = None
    db = get_db()

    id = request.json['id']
    name = request.json['name']
    description = request.json['description']

    # TODO: Validate dashboard name in front-end
    if not name or name == '':
        error = 'Name cannot be empty.'
    
    if error is None:
        try:
            db.execute(
                """
                UPDATE dashboards
                SET name = :name, description = :description
                WHERE id_dashboard = :id;
                """,
                {
                    "name": name,
                    "description": description,
                    "id": id
                }
            )
            db.commit()
            return ({'message': 'Successfully updated dashboard.'}, 201)
        except db.Error as e:
            current_app.logger.error(f'Error when updating board {name} with id {id}!', exc_info=1)
            return ({'error': str(e)}, 500)
    
    elif error is not None:
        return ({'error': error}, 400)


@bp.route('/dashboards/<int:id_dashboard>', methods={'DELETE'})
def delete_dashboard(id_dashboard):
    db = get_db()

    try:
        cur = db.cursor()
        cur.execute("""
            DELETE FROM dashboards
            WHERE id_dashboard = :id_board;
            """,
            {
                "id_board": id_dashboard
            })
        db.commit()

        return ({'deleted': True}, 201)
    # except IntegrityError || ProgrammingError as e:
    except db.Error as e:
        current_app.logger.error('Error when deleting from dashboards table!', exc_info=1)
        return({'error': str(e)}, 500)


#######################
# Datatable related routes

@bp.route('/datatable', methods={'POST'})
def new_datatable():
    error = None
    db = get_db()

    name = request.json['name']
    query = request.json['query']
    id_dashboard = request.json['id_dashboard']

    # TODO: Validate dashboard name in front-end
    if not name or name == '':
        error = 'Name cannot be empty.'
    if not id_dashboard:
        error = 'Dashboard id cannot be empty.'
    
    if error is None:
        try:
            cur = db.cursor()
            cur.execute(
                "INSERT INTO datatables(name, query, id_dashboard) VALUES (?, ?, ?);",
                (name, query, id_dashboard)
            )
            db.commit()
            lastId = cur.lastrowid

            return ({'id_table': lastId}, 200)
        except db.Error as e:
            error = 'Error when inserting into datatables table:\n' + e
    elif error is not None:
        flash(error)
        return (error, 500)


@bp.route('/datatable/<int:id_dashboard>', methods={'GET'})
def get_datatables(id_dashboard):
    error = None
    db = get_db()
    
    try:
        cur = db.cursor()
        data = cur.execute("SELECT id_table, name, query, id_db FROM datatables WHERE id_dashboard = :id_dashboard;", {"id_dashboard": id_dashboard}).fetchall()
        res = []    
        for entry in data:
            res.append({
                'id': entry[0],
                'name': entry[1],
                'query': entry[2],
                'id_db': entry[3]
            })
        
        return jsonify(res)
    except db.Error as e:
        error = 'Error getting data from dashboard tables:\n' + str(e)

    if error is not None:
        current_app.logger.error(f'Failed while getting tables from dashboard with id {id_dashboard}!', exc_info=1)
        return ({'error': error}, 500)


@bp.route('/datatable', methods={'PUT'})
def update_datatable():
    error = None
    db = get_db()

    id_datatable = request.json['id_table']
    name = request.json['name']
    query = request.json['query']
    id_db = request.json['id_db']

    # TODO: Validate dashboard name in front-end
    if not name or name == '':
        error = 'Name cannot be empty.'
    
    if error is None:
        try:
            db.execute(
                """
                UPDATE datatables
                SET name = :name, query = :query, id_db = :id_db
                WHERE id_table = :id_datatable;
                """,
                {
                    "name": name,
                    "query": query,
                    "id_db": id_db,
                    "id_datatable": id_datatable
                }
            )
            db.commit()
            return ({}, 201)
        except db.Error as e:
            current_app.logger.error(f'Error when updating table {name} with id {id_datatable}!', exc_info=1)
            return ({}, 500)
    
    elif error is not None:
        return ({'error': error}, 400)


@bp.route('/datatable/<int:id_datatable>', methods={'DELETE'})
def delete_datatable(id_datatable):
    db = get_db()

    try:
        cur = db.cursor()
        cur.execute("""
            DELETE FROM datatables
            WHERE id_table = :id_table;
            """,
            {
                "id_table": id_datatable
            })
        db.commit()

        return ({'deleted': True}, 201)
    # except IntegrityError || ProgrammingError as e:
    except db.Error as e:
        current_app.logger.error('Error when deleting from datatables table!', exc_info=1)
        return({'error': str(e)}, 500)

