from flaskr.singleton import Singleton
from flaskr.db import get_db
from flask import current_app
from .external_db_factory import ExternalDBFactory
from slugify import slugify


class ExternalDBHandler(metaclass=Singleton):
    """
    Singleton that acts as the DAO for the API.
    Handles all external databases and all their CRUD operations.

    Attributes
    ----------
    _all_created_dbs : dict
        dict holding all instanced databases
    
    Methods
    -------
    load_dbs()
        loads all databases from the stored connections
    create_db(id_db, db_name, db_type, **data)
        asks the factory for a new database instance and stores it
    get_db(db_name, db_type)
        get database of type db_type and name db_name
    update_db(old_db_name, new_db_name, db_type, **data)
        updates an database name (very limited)
    delete_db(id_db)
        deletes a database
    """
    _all_created_dbs = {}
    
    def load_dbs(self):
        db = get_db()
        try:
            cur = db.cursor()
            data = cur.execute(
                """
                SELECT id_db, name, db_type FROM external_dbs;
                """
            ).fetchall()

            for entry in data:
                new_db = ExternalDBFactory().create(entry[0], entry[1], entry[2])
                db_key = f"{entry[2]}_{slugify(entry[1])}"
                self._all_created_dbs[db_key] = new_db
        except db.Error:
            return current_app.logger.error("Error while loading databases!\n", exc_info=1)
    

    def create_db(self, id_db, db_name, db_type, **data):
        new_db_key = f"{db_type}_{slugify(db_name)}"
        new_db = ExternalDBFactory().create(id_db, db_name, db_type, **data)

        if new_db_key not in self._all_created_dbs:
            self._all_created_dbs[new_db_key] = new_db
        else:
            current_app.logger.info(f"Database {new_db_key} already registered!")
        
        return self._all_created_dbs[new_db_key]
    

    def get_db(self, db_name, db_type):
        db_key = f"{db_type}_{slugify(db_name)}"
        current_app.logger.info(f"Getting database {db_key}...")
        db = self._all_created_dbs[db_key]
        if (db):
            return db
        raise ValueError(db_name, db_type)
    

    def update_db(self, old_db_name, new_db_name, db_type, **data):
        old_db_key = f"{db_type}_{slugify(old_db_name)}"
        new_db_key = f"{db_type}_{slugify(new_db_name)}"

        if old_db_key != new_db_key:
            if old_db_key not in self._all_created_dbs:
                current_app.logger.warning(f"Tried to update database {old_db_key}, but it doesn't exist!")
                # raise ValueError
                return
            else:
                current_app.logger.info(f"Updating database {old_db_key}...")
                self._all_created_dbs[new_db_key] = self._all_created_dbs.pop(old_db_key)
                self._all_created_dbs[new_db_key].update_db(new_db_name, **data)
        
        return self._all_created_dbs[new_db_key]
    

    def delete_db(self, id_db):
        for key in self._all_created_dbs:
            if self._all_created_dbs[key].id_db == id_db:
                db = self._all_created_dbs.pop(self._all_created_dbs[key]._g_name)
                db.close_db()
                return
            
        current_app.logger.warning(f"Tried to delete database with id {id_db}, but it wasn't found!")

