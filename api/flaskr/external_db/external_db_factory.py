from flaskr.singleton import Singleton
from flask import current_app
from .db_types import create_or_get_sqlite_db

class ExternalDBFactory(metaclass=Singleton):
    """
    Simple factory that creates database instances.
    For now, only instances SQLite databases.
    
    Attributes
    ----------
    _builders : dict
        dict that stores all registered builders
    
    Methods
    -------
    register_builder(db_type, db_builder)
        registers a new type of builder to the factory
    create(id_db, db_name, db_type, **data)
        creates an db connection instance based on the database type passed
    """
    _builders = {}


    def register_builder(self, db_type, db_builder):
        """
        Registers a new builder to this factory.

        Parameters
        ----------
        db_type : string
            the type of database that the builder will be assigned to
        db_builder : Any
            the builder to be assigned
        
        Returns
        -------
        None
        """
        self._builders[db_type] = db_builder


    def create(self, id_db, db_name, db_type, **data):
        """
        Creates a new database connection instance. The type depends on the db_type parameter and on
        the builder that it is associated to.
        
        Parameters
        ----------
        id_db : int
            the database id of this database
        db_name : str
            the name of this database
        db_type : str
            the key of this database type
        **data : Any, optional
            any other non-keyword arguments to be passed
        
        Returns
        -------
        new_db : Any
            an object containing the connection instance to the db
        
        Raises
        ------
        ValueError
            on unregistered database type
        """
        # Get database builder, or return error if it isn't supported.
        builder = self._builders[db_type]
        if not builder:
            current_app.logger.error(f"Tried to create database of type {db_type}, but it isn't supported!")
            raise ValueError(db_type)
        
        # Return the created database.   
        current_app.logger.info(f"Creating/Registering new database {db_name} of type {db_type}...")
        new_db = builder(id_db=id_db, db_name=db_name, **data)
        return new_db
