import os
import sqlite3
from pathlib import PurePath
from flaskr.db import get_db

from flask import current_app, g
from slugify import slugify

class SQLiteDB:
    """
    Class that holds data about a Sqlite database connection.
    Uses the flask g object.
    See https://flask.palletsprojects.com/en/2.0.x/appcontext/

    ...
    Attributes
    ----------
    id_db : int
        database id (from the external_dbs table)
    name : str
        database name
    _g_name : str
        database key used in the g object, follows the format sqlite_normalizedName
    path : str
        path to the database in the filesystem

    """
    def __init__(self, id_db, name):
        """
        Receives some attributes, and constructs some more.

        Parameters
        ----------
                id_db (int): Database id on the external_dbs table;
                name (str): Database name.
        """

        self.id_db = id_db # DB ID on non-specific db table
        self.name = name
        self._g_name = f'sqlite_{slugify(self.name)}'
        self.path = os.path.join(current_app.root_path, f'sql\external_dbs\{slugify(self.name)}.db')
        self.init_db()
    

    def get_db(self):
        """
        Returns an connection instance of this sqlite database

        Returns
        -------
        db : sqlite3.Connection
            a connection instance to this database
        """
        if self._g_name not in g:
            # Uses URI to allow for using read-write-create mode (?mode=rwc).
            db_path = PurePath(self.path).as_uri() + '?mode=rwc'
            setattr(g, self._g_name, sqlite3.connect(
                db_path,
                detect_types=sqlite3.PARSE_DECLTYPES,
                uri=True
            ))

            getattr(g, self._g_name).row_factory = sqlite3.Row
        return getattr(g, self._g_name)

    def update_db(self, name, **_):
        """
        Updates the database. At present, only updates its name, and doesn't change its path
        nor its filename. Accepts extra, unused arguments, to allow for generic calls to this function.

        Parameters
        ----------
        name : str
            new database name
        **_ : Any
            ignored arguments

        Returns
        -------
        None
        """
        self.close_db()
        # TODO: Create new database file, update it with old db file content and delete old db file.
        has_changed = False

        if (self.name != name):
            self.name = name
            self._g_name = f'sqlite_{slugify(self.name)}'
            # self.path = os.path.join(current_app.root_path, f'sql\external_dbs\{slugify(self.name)}.db')
            has_changed = True

        if has_changed:
            db = get_db() # internal db, not instanced external
            try:
                db.execute("""
                    UPDATE external_sqlite_dbs
                    SET name = :name
                    WHERE id_db = :id_db;
                    """,
                    {
                        "name": self.name,
                        "id_db": self.id_db
                    })
                db.commit()

            except db.Error as e:
                current_app.logger.error('Error when updating external sqlite databases table!', exc_info=1)
                return db.error
        
            # Runs get method to reinitialize connection.
            self.get_db()

    def close_db(self, *_):
        """
        Closes this database instance. Called on the end of an application context.
        """
        db = g.pop(self._g_name, None)

        if db is not None:
            db.close()

    def delete_db(self):
        """
        Function to completely remove the database from the application.
        At present only closes the database.

        Returns
        -------
        None
        """
        # TODO: Delete DB from filesystem
        # NOTE: No need to remove from external_sqlite_dbs, as it will be removed
        # by cascade when it is deleted from the external_dbs table.
        self.close()

    def init_db(self):
        """
        Initializes this database, while also registering itself on the internal database.

        Returns
        -------
        None
        """
        db = get_db() # internal db, not instanced external
        try:
            db.execute(
                "INSERT INTO external_sqlite_dbs (name, id_db) VALUES (?, ?);",
                (self.name, self.id_db)
            )
            db.commit()
        except db.IntegrityError as e:
            current_app.logger.warning('Db is already registered! Skipping.')
        except db.Error as e:
            current_app.logger.error('Error when inserting into external sqlite databases table!', exc_info=1)
            return e
        

def create_or_get_sqlite_db(id_db, db_name, **_ignored):
    """
    Builder function that creates an returns SQLiteDB object.

    Parameters
    ----------
    id_db : int
        the database's id (from the external_dbs table)
    db_name : str
        the database's name
    """
    return SQLiteDB(id_db=id_db, name=db_name)
