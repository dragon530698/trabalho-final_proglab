class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        Simple singleton pattern. Instances an object of a generic class when
        that class is called, and extends this class (via metadata=Singleton).

        Parameters
        ----------
        *args : Any, optional
            Any number of non-keyword arguments;
        **kwargs : Any, optional
            Any number of keyword arguments.
        """

        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]