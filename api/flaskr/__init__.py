from os import path
from dotenv import load_dotenv

from flask import Flask
from .blueprints import main_bp, external_db_bp

from . import db
from .external_db.db_types import create_or_get_sqlite_db
from .external_db.external_db_handler import ExternalDBHandler
from .external_db.external_db_factory import ExternalDBFactory


def create_app():
    """
    Initializes this flask application and all the required modules. This is where it all begins.

    Returns
    -------
    app : Flask.flask
        Flask object that runs the application.
    """

    load_dotenv('.flaskenv')

    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=path.abspath(path.join(path.dirname(__file__), 'sql\internal.db')),
    )

    # if test_config is None:
    #     # load the instance config, if it exists, when not testing
    #     app.config.from_pyfile('config.py', silent=True)
    # else:
    #     # load the test config if passed in
    #     app.config.from_mapping(test_config)

    # # ensure the instance folder exists
    # try:
    #     os.makedirs(app.instance_path)
    # except OSError:
    #     pass

    ExternalDBFactory().register_builder(db_type='sqlite', db_builder=create_or_get_sqlite_db)

    with app.app_context():
        app.logger.info("Initializing internal database...")
        db.init_db()

        app.logger.info("Registering supported database builders to factory...")
        ExternalDBHandler().load_dbs()

    # Registering builders on external db factory

    app.logger.info("Registering blueprints...")
    app.register_blueprint(main_bp.bp)
    app.register_blueprint(external_db_bp.bp)

    return app