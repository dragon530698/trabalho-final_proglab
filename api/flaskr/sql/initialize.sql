CREATE TABLE IF NOT EXISTS dashboards(
    id_dashboard INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS datatables(
    id_table INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(255) NOT NULL,
    query TEXT,
    id_dashboard INTEGER NOT NULL,
    id_db INTEGER REFERENCES external_dbs(id_db),
    CONSTRAINT fk_dashboard
        FOREIGN KEY (id_dashboard)
        REFERENCES dashboards(id_dashboard)
        ON DELETE CASCADE,
    CONSTRAINT fk_db
        FOREIGN KEY (id_db)
        REFERENCES external_dbs(id_db)
        ON DELETE SET NULL
);

CREATE TABLE IF NOT EXISTS external_dbs(
    id_db INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(255) NOT NULL UNIQUE,
    db_type VARCHAR(32) NOT NULL
);

CREATE TABLE IF NOT EXISTS external_sqlite_dbs(
    id_sqlite_db INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(255) NOT NULL UNIQUE,
    id_db INTEGER NOT NULL,
    CONSTRAINT fk_db
        FOREIGN KEY (id_db)
        REFERENCES external_dbs(id_db)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS external_postgres_dbs(
    id_pg_db INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(255) NOT NULL UNIQUE,
    host VARCHAR(255) NOT NULL,
    db VARCHAR(255) NOT NULL,
    user VARCHAR(255) NOT NULL,
    pass VARCHAR(255),
    id_db INTEGER NOT NULL,
    CONSTRAINT fk_db
        FOREIGN KEY (id_db)
        REFERENCES external_dbs(id_db)
        ON DELETE CASCADE
);


-- JOIN external_postgres_dbs AS postgres_dbs ON postgres.id_db = external_dbs.id_db