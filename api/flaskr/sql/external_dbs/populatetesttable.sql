CREATE TABLE IF NOT EXISTS pizza_tipo (
    id_tipo INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS ingredientes (
    id_ingrediente INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS pizza (
    id_pizza INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(50) NOT NULL,
    price REAL NOT NULL,
    id_tipo INTEGER REFERENCES pizza_tipo(id_tipo)
);

CREATE TABLE IF NOT EXISTS relacao_pizza_ingrediente (
    id_relacao INTEGER PRIMARY KEY AUTOINCREMENT,
    id_pizza INTEGER NOT NULL REFERENCES pizza(id_pizza),
    id_ingrediente INTEGER NOT NULL REFERENCES ingredientes(id_ingrediente)
);

INSERT INTO pizza_tipo(name)
VALUES ('Padrão'), ('Doce'), ('Especial');

INSERT INTO ingredientes(name)
VALUES ('Pepperonni'),('Queijo Prato'), ('Mussarela'), ('Gorgonzola'), ('Parmesão'), ('Tomate'), ('Lombo'), ('Ovo'), ('Cebola'), ('Presunto'), ('Chocolate'), ('Azeitona');

INSERT INTO pizza(name, price, id_tipo)
VALUES ('Pepperonni', 13.50, (SELECT id_tipo FROM pizza_tipo WHERE name = 'Especial')),
    ('Quatro queijos', 12.50, (SELECT id_tipo FROM pizza_tipo WHERE name = 'Padrão')),
    ('Portuguesa', 14.50, (SELECT id_tipo FROM pizza_tipo WHERE name = 'Padrão')),
    ('Lombo', 13.50, (SELECT id_tipo FROM pizza_tipo WHERE name = 'Padrão')),
    ('Chocolate', 9.50, (SELECT id_tipo FROM pizza_tipo WHERE name = 'Doce'));

INSERT INTO relacao_pizza_ingrediente(id_pizza, id_ingrediente)
VALUES ((SELECT id_pizza FROM pizza WHERE name = 'Pepperonni'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Pepperonni')),
        ((SELECT id_pizza FROM pizza WHERE name = 'Pepperonni'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Mussarela')),
        ((SELECT id_pizza FROM pizza WHERE name = 'Pepperonni'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Cebola')),

        ((SELECT id_pizza FROM pizza WHERE name = 'Lombo'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Lombo')),
        ((SELECT id_pizza FROM pizza WHERE name = 'Lombo'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Mussarela')),
        ((SELECT id_pizza FROM pizza WHERE name = 'Lombo'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Tomate')),

        ((SELECT id_pizza FROM pizza WHERE name = 'Quatro queijos'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Mussarela')),
        ((SELECT id_pizza FROM pizza WHERE name = 'Quatro queijos'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Gorgonzola')),
        ((SELECT id_pizza FROM pizza WHERE name = 'Quatro queijos'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Queijo Prato')),
        ((SELECT id_pizza FROM pizza WHERE name = 'Quatro queijos'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Parmesão')),

        ((SELECT id_pizza FROM pizza WHERE name = 'Portuguesa'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Ovo')),
        ((SELECT id_pizza FROM pizza WHERE name = 'Portuguesa'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Cebola')),
        ((SELECT id_pizza FROM pizza WHERE name = 'Portuguesa'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Tomate')),
        ((SELECT id_pizza FROM pizza WHERE name = 'Portuguesa'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Azeitona')),
        ((SELECT id_pizza FROM pizza WHERE name = 'Portuguesa'),
        (SELECT id_ingrediente FROM ingredientes WHERE name = 'Presunto'));
