INSERT INTO dashboards(name, description)
VALUES ('Dashboard 1', 'This is an example dashboard'),
    ('Dashboard 2', 'This is an example dashboard');

INSERT INTO external_dbs(name, db_type)
VALUES ('TestDB1', 'sqlite');

INSERT INTO external_sqlite_dbs(name, id_db)
VALUES ('TestDB1', (SELECT id_db FROM external_dbs WHERE name='TestDB1'));

INSERT INTO datatables(name, query, id_dashboard, id_db)
VALUES ('Example 1', '', (SELECT id_dashboard FROM dashboards WHERE name='Dashboard 1'), (SELECT id_db FROM external_dbs WHERE name='TestDB1')),
('Example 2', '', (SELECT id_dashboard FROM dashboards WHERE name='Dashboard 1'), (SELECT id_db FROM external_dbs WHERE name='TestDB1')),
('Example 3', '', (SELECT id_dashboard FROM dashboards WHERE name='Dashboard 2'), (SELECT id_db FROM external_dbs WHERE name='TestDB1')),
('Example 4', '', (SELECT id_dashboard FROM dashboards WHERE name='Dashboard 2'), (SELECT id_db FROM external_dbs WHERE name='TestDB1'));