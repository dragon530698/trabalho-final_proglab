# Fino - API

Simples sistema de analise de dados. (API)

------------------------------------------------

## Instalando dependências
```bash
pip install -r requirements.txt
OR
python -m pip install -r requirements.txt
```

### Start the app
```bash
flask run
OR
python -m ./api/main.py
```
