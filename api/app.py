import flaskr


if __name__ == '__main__':
    '''
    Initializes the API server. This is merely a proxy.
    '''

    # https://stackoverflow.com/questions/38982807/are-a-wsgi-server-and-http-server-required-to-serve-a-flask-app/38982989#38982989
    # TODO: Use with a proper WSGI server;
    app = flaskr.create_app().run(debug=False)